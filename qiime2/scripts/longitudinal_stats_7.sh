#!/bin/bash

mkdir ../analysis/pairwise-tests

#create table of relative frequency:
qiime feature-table relative-frequency --i-table ../data/table.qza --o-relative-frequency-table ../data/rel_freq_table.qza


# Doing a pairwise-analysis of shannon
qiime longitudinal pairwise-differences \
  --m-metadata-file ../data/metadata.tsv \
  --m-metadata-file ../analysis/core-metrics-results3/shannon.qza \
  --p-metric shannon \
  --p-group-column diet \
  --p-state-column months \
  --p-state-1 0 \
  --p-state-2 12 \
  --p-individual-id-column Id.patient \
  --p-replicate-handling random \
  --o-visualization ../analysis/pairwise-differences.qzv

# Doing a pairwise-analysis of bacteroidetes
qiime longitudinal pairwise-differences \
  --m-metadata-file ../data/metadata.tsv \
  --m-metadata-file ../data/OTU_level2.tsv \
  --p-metric "k__Bacteria;p__Bacteroidetes" \
  --p-group-column diet \
  --p-state-column months \
  --p-state-1 0 \
  --p-state-2 12 \
  --p-individual-id-column Id.patient \
  --p-replicate-handling random \
  --o-visualization ../analysis/pairwise-tests/bateroidetes_pw.qzv

# Doing a pairwise-analysis of firmicutes
qiime longitudinal pairwise-differences \
  --m-metadata-file ../data/metadata.tsv \
  --m-metadata-file ../data/OTU_level2.tsv \
  --p-metric "k__Bacteria;p__Firmicutes" \
  --p-group-column diet \
  --p-state-column months \
  --p-state-1 0 \
  --p-state-2 12 \
  --p-individual-id-column Id.patient \
  --p-replicate-handling random \
  --o-visualization ../analysis/pairwise-tests/firmicutes_pw.qzv


# Doing a pairwise-analysis of actinobacteria
qiime longitudinal pairwise-differences \
  --m-metadata-file ../data/metadata.tsv \
  --m-metadata-file ../data/OTU_level2.tsv \
  --p-metric "k__Bacteria;p__Actinobacteria" \
  --p-group-column diet \
  --p-state-column months \
  --p-state-1 0 \
  --p-state-2 12 \
  --p-individual-id-column Id.patient \
  --p-replicate-handling random \
  --o-visualization ../analysis/pairwise-tests/actinobact_pw.qzv

# Doing a pairwise-analysis of prevotella
qiime longitudinal pairwise-differences \
  --m-metadata-file ../data/metadata.tsv \
  --m-metadata-file ../data/OTU_level6.tsv \
  --p-metric "k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella" \
  --p-group-column diet \
  --p-state-column months \
  --p-state-1 0 \
  --p-state-2 12 \
  --p-individual-id-column Id.patient \
  --p-replicate-handling random \
  --o-visualization ../analysis/pairwise-tests/prevotella.qzv

# Doing a pairwise-analysis of prevotella compared by debut of diabetes
qiime longitudinal pairwise-differences \
  --m-metadata-file ../data/metadata.tsv \
  --m-metadata-file ../data/OTU_level6.tsv \
  --p-metric "k__Bacteria;p__Bacteroidetes;c__Bacteroidia;o__Bacteroidales;f__Prevotellaceae;g__Prevotella" \
  --p-group-column debut \
  --p-state-column months \
  --p-state-1 0 \
  --p-state-2 12 \
  --p-individual-id-column Id.patient \
  --p-replicate-handling random \
  --o-visualization ../analysis/pairwise-tests/debut_prevotella.qzv

# Doing a pairwise-distance analysis of shannon
qiime longitudinal pairwise-distances \
  --i-distance-matrix ../analysis/core-metrics-results3/weighted_unifrac_distance_matrix.qza \
  --m-metadata-file ../data/metadata.tsv \
  --p-group-column diet \
  --p-state-column months \
  --p-state-1 0 \
  --p-state-2 12 \
  --p-individual-id-column Id.patient \
  --p-replicate-handling random \
  --o-visualization ../analysis/pairwise-distances.qzv



# volatility analysis:
qiime longitudinal feature-volatility \
  --i-table ../data/table.qza \
  --m-metadata-file ../data/metadata.tsv \
  --p-state-column months \
  --p-individual-id-column Id.patient \
  --p-n-estimators 10 \
  --p-random-state 17 \
  --output-dir ../analysis/feat-volatility












