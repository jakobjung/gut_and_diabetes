#!/bin/bash

echo "create alpha rarefaction plot"

qiime diversity alpha-rarefaction \
      --i-table ../core-metrics-results3/rarefied_table.qza \
      --p-max-depth 35000 \
      --m-metadata-file ../data/mapping_file.tsv \
      --o-visualization ../analysis/alpha_rarefaction.qzv

# qiime tools view ../analysis/alpha_rarefaction.qzv
