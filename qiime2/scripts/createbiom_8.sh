#!/bin/bash

qiime tools export \
      --input-path ../data/taxonomy_vsearch_silva132.qza \
      --output-path ../../data/exported_bioms

qiime tools export \
      --input-path ../data/table.qza \
      --output-path ../../data/exported_bioms

# export tree:
qiime tools export \
  --input-path ../data/rooted-tree.qza \
  --output-path ../../data/exported_bioms

mv ../../data/exported_bioms/tree.nwk \
   ../../data/exported_bioms/rooted_tree_qiime.nwk


cp ../../data/exported_bioms/taxonomy.tsv \
   ../../data/exported_bioms/biom-taxonomy_qiime132.tsv


# Change the biom-taxonomy_qiime132 tsv file headers as:
# #OTUID   taxonomy   confidence in a .txt file

biom add-metadata \
     -i ../../data/exported_bioms/feature-table.biom \
     -o ../../data/exported_bioms/table_w_taxonomy_qiime132.biom \
     --observation-metadata-fp ../../data/exported_bioms/biom-taxonomy_qiime132.txt \
     --sc-separated taxonomy
