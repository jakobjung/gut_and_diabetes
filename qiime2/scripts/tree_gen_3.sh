#!/bin/bash

# generate a tree:
qiime phylogeny align-to-tree-mafft-fasttree \
  --i-sequences ../data/rep-seqs.qza \
  --o-alignment ../data/aligned-rep-seqs.qza \
  --o-masked-alignment ../data/masked-aligned-rep-seqs.qza \
  --o-tree ../data/unrooted-tree.qza \
  --o-rooted-tree ../data/rooted-tree.qza
