#!/bin/bash

# use SILVA-classifier and vsearch algorithm:
qiime feature-classifier classify-consensus-vsearch \
  --i-query ../data/rep-seqs.qza \
  --i-reference-reads ../data/silva_128_reads_99_unaligned.qza \
  --i-reference-taxonomy ../data/silva_128_tax_99_7l.qza \
  --p-perc-identity 0.99 \
  --o-classification ../data/taxonomy_vsearch_silva.qza \
  --p-threads 12 --verbose

# make taxonomy table viewable:
qiime metadata tabulate \
      --m-input-file ../data/taxonomy_vsearch_silva.qza  \
      --o-visualization ../analysis/taxonomy_vsearch_silva.qzv

#create barplots:
qiime taxa barplot \
      --i-table ../data/table.qza \
      --i-taxonomy ../data/taxonomy_vsearch_silva.qza \
      --m-metadata-file ../data/metadata.tsv \
      --o-visualization ../analysis/taxa-bar-plots_silva1.qzv

#export OTU table:
qiime tools export --input-path ../data/table.qza --output-path ../downstream_analysis/data

# OTU tables exports as feature-table.biom so convert to .tsv
biom convert -i ../downstream_analysis/data/feature-table.biom -o ../downstream_analysis/data/otu-table.txt --to-tsv
# Manually change #OTUID to OTUID

# export taxonomy to same folder:
qiime tools export --input-path ../data/taxonomy_vsearch_silva.qza --output-path ../downstream_analysis/data
# Manually change feature id to OTUID

#export tree
qiime tools export --input-path ../data/unrooted-tree.qza --output-path ../downstream_analysis/data


# from now on, the rest downstream analysis will be done with R



