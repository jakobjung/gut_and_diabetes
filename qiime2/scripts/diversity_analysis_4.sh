#!/bin/bash

# create a folder of diversity analysis:
qiime diversity core-metrics-phylogenetic \
  --i-phylogeny ../data/rooted-tree.qza \
  --i-table ../data/table.qza \
  --p-sampling-depth 35000 \
  --m-metadata-file ../data/metadata.tsv \
  --output-dir ../core-metrics-results3
  --o-shannon-vector ../core-metrics-results3/shannon.qza

# check out microbial composition. Assosiations bret metatada and alp div:
qiime diversity alpha-group-significance \
  --i-alpha-diversity ../core-metrics-results3/faith_pd_vector.qza \
  --m-metadata-file ../data/metadata.tsv \
  --o-visualization ../core-metrics-results3/faith-pd-group-significance.qzv

# same for evenness:
qiime diversity alpha-group-significance \
  --i-alpha-diversity ../core-metrics-results3/evenness_vector.qza \
  --m-metadata-file ../data/metadata.tsv \
  --o-visualization ../core-metrics-results3/evenness-group-significance.qzv

# PERMANOVA for beta diversity:
qiime diversity beta-group-significance \
  --i-distance-matrix ../core-metrics-results3/unweighted_unifrac_distance_matrix.qza \
  --m-metadata-file ../data/metadata.tsv \
  --m-metadata-column diet_time \
  --o-visualization ../core-metrics-results3/unweighted-unifrac-body-site-significance.qzv \
  --p-pairwise
