#!/bin/bash

# run the denoising algorithm using dada2:
# trimmed first 13 positions (lower quality)
# not truncated, because the ends need to overlap!
echo 'starting denoising'
qiime dada2 denoise-paired \
  --i-demultiplexed-seqs ../data/fastq_data_paired-end.qza \
  --p-trim-left-f 13 \
  --p-trim-left-r 13 \
  --p-trunc-len-f 240 \
  --p-trunc-len-r 160 \
  --o-table ../data/table.qza \
  --o-representative-sequences ../data/rep-seqs.qza \
  --o-denoising-stats ../data/denoising-stats.qza
  --verbose
echo 'Denoising has been accomplished'
# you can use more threads for faster processing with:
# --p-n-threads (def 1. using too many threads makes computer slowww!)
# in the denoise-paired command!
# also you can decrease nr of reads for training model:
# --p-n-reads-learn (default 1,000,000)  

echo get "denoising stats"
qiime metadata tabulate \
--m-input-file ../data/denoising-stats.qza \
--o-visualization ../analysis/denoising-stats.qzv
# qiime tools view ../analysis/denoising-stats.qzv




# now we generate summaries of the denoising process:
echo 'Summarizing outputs'
qiime feature-table summarize \
  --i-table ../data/table.qza \
  --o-visualization ../analysis/table.qzv \
  --m-sample-metadata-file ../data/metadata.tsv
# visualize feature table:
qiime feature-table tabulate-seqs \
  --i-data ../data/rep-seqs.qza \
  --o-visualization ../analysis/rep-seqs.qzv

# now you can view summaries with qiime tools view fastq_data_paired-end.qzv

