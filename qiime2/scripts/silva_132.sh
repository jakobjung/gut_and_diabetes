#!/bin/bash

# import the unaligned sequences of qiime 132:
qiime tools import\
      --input-path ../data/Silva_132_release/SILVA_132_QIIME_release/rep_set/rep_set_16S_only/99/silva_132_99_16S.fna\  # 99% similarity for low error
      --output-path ../data/seqs_132.qza\
      --type FeatureData[Sequence]


# import taxonomy of qiime 132:
qiime tools import\
      --input-path ../data/Silva_132_release/SILVA_132_QIIME_release/taxonomy/16S_only/99/taxonomy_7_levels.txt\
      --output-path ../data/taxonomy_132.qza\
      --type FeatureData[Taxonomy]\
      --input-format HeaderlessTSVTaxonomyFormat

# use SILVA-classifier and vsearch algorithm:
qiime feature-classifier classify-consensus-vsearch \
  --i-query ../data/rep-seqs.qza \
  --i-reference-reads ../data/seqs_132.qza \
  --i-reference-taxonomy ../data/taxonomy_132.qza \
  --p-perc-identity 0.99 \
  --o-classification ../data/taxonomy_vsearch_silva132.qza \
  --p-threads 12 \
  --verbose

# Output: 3060 matching query sequences (54.88 %)

# make taxonomy table viewable:
qiime metadata tabulate \
      --m-input-file ../data/taxonomy_vsearch_silva132.qza  \
      --o-visualization ../analysis/taxonomy_vsearch_silva132.qzv

#create barplots:
qiime taxa barplot \
      --i-table ../data/table.qza \
      --i-taxonomy ../data/taxonomy_vsearch_silva132.qza \
      --m-metadata-file ../data/metadata.tsv \
      --o-visualization ../analysis/taxa-bar-plots_silva132.qzv

#export OTU table:
qiime tools export --input-path ../data/table.qza --output-path ../downstream_analysis/data

# OTU tables exports as feature-table.biom so convert to .tsv
biom convert -i ../downstream_analysis/data/feature-table.biom -o ../downstream_analysis/data/otu-table.txt --to-tsv
# Manually change #OTUID to OTUID

# export taxonomy to same folder:
qiime tools export --input-path ../data/taxonomy_vsearch_silva132.qza --output-path ../downstream_analysis/data
# Manually change feature id to OTUID

#export tree
qiime tools export --input-path ../data/rooted-tree.qza --output-path ../downstream_analysis/data/


# from now on, the rest downstream analysis will be done with R








