#!/bin/bash

# importing data to the directory to apply qiime tools in qza format
qiime tools import \
  --type 'SampleData[PairedEndSequencesWithQuality]' \
  --input-path ../../data/fastqfiles_gz \
  --input-format CasavaOneEightSingleLanePerSampleDirFmt \
  --output-path ../data/fastq_data_paired-end.qza

# create a visualization of quality parameters of fastqs (like fastqc)
qiime demux summarize \
      --i-data ../data/fastq_data_paired-end.qza \
      --o-visualization ../analysis/fastq_data_paired-end.qzv

# you can view the qzv file with:
# qiime tools view fastq_data_paired-end.qzv
