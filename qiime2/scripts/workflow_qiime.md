Title: Workflow Qiime 2 gut microbiota and diabetes during a sardine-rich diet

Author: Jakob Jung

Date: June 7, 2019



# Initialization

Here, we explain in detail the Qiime2 workflow that I used in order to analyse the 16S rRNA data provided as Fastq-files. Firstly we need to go to scripts directory and set up the qiime2 virtual machine in order to conduct qiime commands (This command always needs to be made to access qiime 2):

```bash
cd ~/Documents/gut_diabetes/qiime2/scripts
source activate qiime2-2019.4
```

Then we import all fastq demultiplexed paired-end reads from the zipped file "fastqfiles_gz". The output file is a qiime artifact (.qza) which can be used in qiime 2: 

```bash
qiime tools import \
  --type 'SampleData[PairedEndSequencesWithQuality]' \
  --input-path ../../data/fastqfiles_gz \
  --input-format CasavaOneEightSingleLanePerSampleDirFmt \
  --output-path ../data/fastq_data_paired-end.qza
```

Next, we create a visualization of the quality of the reads (similar to fastqc). The visualization file (.qzv) can be opened as html file using the "view" tool of qiime:

```bash
qiime demux summarize \
      --i-data ../data/fastq_data_paired-end.qza \
      --o-visualization ../analysis/fastq_data_paired-end.qzv
qiime tools view ../analysis/fastq_data_paired-end.qzv
```

![seq_counts_summary](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/seq_counts_summary.png)

![sequence_counts](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/sequence_counts.png)

Forward reads:

![fastqc_fw](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/fastqc_fw.png)

Reverse reads:

![fastqc_rv](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/fastqc_rv.png)

As expected, the quality drops in the ends of both the forward and reverse reads. The forward reads are generally of higher quality. We can use this information in the following steps for trimming and truncating the reads. 

The sequences are paired-end and do not need to be merged, because DADA2 performs merging automatically later.



# Denoising

Now we denoise the sequences to remove and correct noisy reads. Here we use the method "DADA2". It basically filters out noisy sequences, corrects error of noisy sequences, removes chimeric sequences, removes singletons, joins denoised paired-end reads, and dereplicates the sequences. 

We applied the DADA 2 denoising function for paired-end reads. We trim both forward and reverse reads at 13 because the quality starts off with a lower quality (see previous 2 plots). The end of forward reads is truncated at 240 and of reverse reads at 160, because the reverse reads decrease in quality at an earlier point. Because we use 2x250 V4 sequence data, the forward and reverse sequences almost completely overlap and can be therefore be trimmed completely based on the quality scores. 

As output, we get denoising stats, representative sequences (feature data) and a feature table:

```bash
qiime dada2 denoise-paired \
  --i-demultiplexed-seqs ../data/fastq_data_paired-end.qza \
  --p-trim-left-f 13 \
  --p-trim-left-r 13 \
  --p-trunc-len-f 240 \
  --p-trunc-len-r 160 \
  --o-table ../data/table.qza \
  --o-representative-sequences ../data/rep-seqs.qza \
  --o-denoising-stats ../data/denoising-stats.qza
  --verbose
```

 Now that the denoising has been accomplished (takes a while), we can create some qiime2 visualizations of the outcomes:

### Denoising stats:

```bash
qiime metadata tabulate \
  --m-input-file ../data/denoising-stats.qza \
  --o-visualization ../analysis/denoising-stats.qzv
qiime tools view ../analysis/denoising-stats.qzv
```

![denoising_stats](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/denoising_stats.png)

Above we can see per sample, how many reads are left after running the denoising algorithm  of DADA2. The data looks reasonable since a high amount of reads is retained.

### Feature table:

Now we can work with the feature table (OTU) which includes counts for each taxon:

```bash
qiime feature-table summarize \
  --i-table ../data/table.qza \
  --o-visualization ../analysis/table.qzv \
  --m-sample-metadata-file ../data/metadata.tsv
qiime tools view ../analysis/table.qzv
```

![featurecounts](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/featurecounts.png)

Feature counts are listed. it shows how many sequences are associated with each sample. The lowest feature count was at 360 000. 

### Representative sequences (Feature data):

```bash
qiime feature-table tabulate-seqs \
  --i-data ../data/rep-seqs.qza \
  --o-visualization ../analysis/rep-seqs.qzv
qiime tools view ../analysis/rep-seqs.qzv
```

![rep-seqs](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/rep-seqs.png)



<a name="seqtab"></a>Sequence - table:

![rep-seqs2](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/rep-seqs2.png)

Here we get statistics on sequence lengths, and in the last table, a mapping of feature ids to sequences. These sequences can then easily be run in BLAST to find out the associated microbes (e.g. the first ID refers to Bacteroides). They can be connected to the OTU table and are used later for taxonomic assignment.



# Generating a phylogenetic tree

Now we can use the representative sequences file to generate a tree in a qiime artifact. The pipeline "align-to-tree-mafft-fasttree" uses the mafft program to perform a multiple sequence alignment. Then it masks the alignment to remove variable positions. Then it applies FastTree to generate the unrooted and rooted phylogenetic trees of the masked alignment:

```bash
qiime phylogeny align-to-tree-mafft-fasttree \
  --i-sequences ../data/rep-seqs.qza \
  --o-alignment ../data/aligned-rep-seqs.qza \
  --o-masked-alignment ../data/masked-aligned-rep-seqs.qza \
  --o-tree ../data/unrooted-tree.qza \
  --o-rooted-tree ../data/rooted-tree.qza
```

 The tree is important for the subsequent Diversity analysis, since some metrics (e.g. Faith PD measure the phylogenetic distances within and between samples).



# Diversity analysis

We can do diversity analyses using QIIME 2's diversity plugin. It can be used to compute alpha and beta diversity data, applying statistics, and generating visualizations. "core-metrics-phylogenetic" rarefies the feature table to the depth we defined (35000, because the lowest frequency was around 36000) and computes many alpha and beta diversity metrics and visualizations by default:

```bash
qiime diversity core-metrics-phylogenetic \
  --i-phylogeny ../data/rooted-tree.qza \
  --i-table ../data/table.qza \
  --p-sampling-depth 35000 \
  --m-metadata-file ../data/metadata.tsv \
  --output-dir ../core-metrics-results3
  --o-shannon-vector ../core-metrics-results3/shannon.qza
```

The output-visualizations of alpha and beta diversity are shown below. They can be adjusted by samples and different parameters of the metadata. More metrics are available than the shown, and can be viewed manually.



### Alpha diversity:

Faith’s Phylogenetic Diversity (qualitiative measure of community richness that incorporates phylogenetic relationships between the features):

```bash
qiime diversity alpha-group-significance \
  --i-alpha-diversity ../core-metrics-results3/faith_pd_vector.qza \
  --m-metadata-file ../data/metadata.tsv \
  --o-visualization ../core-metrics-results3/faith-pd-group-significance.qzv
qiime tools view ../core-metrics-results3/faith-pd-group-significance.qzv
```

Alpha diversity boxplots:

![fpd_sign_bp](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/fpd_sign_bp.png)

![kw-test](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/kw-test.png)



It shows that no significant changes in alpha-diversity were found by the Kruskal-Wallis test. It was expected, since only few changes due to the change of diet were expected. 



Evenness (or Pielou’s Evenness; measure of community evenness):

```bash
qiime diversity alpha-group-significance \
  --i-alpha-diversity ../core-metrics-results3/evenness_vector.qza \
  --m-metadata-file ../data/metadata.tsv \
  --o-visualization ../core-metrics-results3/evenness-group-significance.qzv
qiime tools view ../core-metrics-results3/evenness-group-significance.qzv
```

![evenness](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/evenness.png)

Also in evenness, no significant differences were found. 



### Beta diversity:

In the following plots, red points represents subjects consuming a sardine-rich diet before the study and blue points represent the same subjects after one year of sardine-rich diet:

Jaccard distance  (qualitative measure of community dissimilarity):

```bash
qiime tools view ../core-metrics-results3/jaccard_emperor.qzv 
```

![jaccard_e](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/jaccard_e.svg)



Bray-Curtis distance (quantitative measure of community dissimilarity):

```bash
qiime tools view ../core-metrics-results3/bray_curtis_emperor.qzv 
```

![bray-curtis_e](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/bray-curtis_e.svg)



Unweighted UniFrac distance (qualitative measure of community dissimilarity that incorporates phylogenetic relationships between the features):

```bash
qiime tools view ../core-metrics-results3/unweighted_unifrac_emperor.qzv
```

![unwe_unifrac_e](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/unwe_unifrac_e.svg)



Weighted UniFrac distance (quantitative measure of community dissimilarity that incorporates phylogenetic relationships between the features):

```bash
qiime tools view ../core-metrics-results3/weighted_unifrac_emperor.qzv 
```

![wei_unifrac_e](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/wei_unifrac_e.svg)



Now we will analyze beta diversity using a PERMANOVA test with the beta-group-significance command:

```bash
qiime diversity beta-group-significance \
  --i-distance-matrix ../core-metrics-results3/unweighted_unifrac_distance_matrix.qza \
  --m-metadata-file ../data/metadata.tsv \
  --m-metadata-column diet_time \
  --o-visualization ../core-metrics-results3/unweighted-unifrac-body-site-significance.qzv \
  --p-pairwise
qiime tools view ../core-metrics-results3/unweighted-unifrac-body-site-significance.qzv 
```

![group_sign_PERMANOVA](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/group_sign_PERMANOVA.png)

Also here, no significant changes can be found between the different diets. This lets us conclude that there must be no clear changes in diversity metrics induced by diet.



# Alpha rarefaction curves

Here, we use rarefaction plots  to see the numbers of OTUs (or ASVs in this case) at various sequencing depths. This is done to show that the chosen cutoff was reasonable.

```bash
qiime diversity alpha-rarefaction \
      --i-table ../core-metrics-results3/rarefied_table.qza \
      --p-max-depth 35000 \
      --m-metadata-file ../data/mapping_file.tsv \
      --o-visualization ../analysis/alpha_rarefaction.qzv
qiime tools view ../analysis/alpha_rarefaction.qzv
```

![rarefraction](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/rarefraction.png)



The plot shows that the defined sequencing depth of 35000 is sufficient, and no samples have to be excluded. 



# Taxonomic analysis

Of course we have to find out which microorganisms are present in which proportion in the different samples. 



 ## Analyse taxonomy

For assigning taxonomy, we are using the VSEARCH algorithm to align the reads to the SILVA database (version 128). The VSEARCH algorithm is used because the SILVA database is very big and other algorithms such as the naive Bayes classifier require a too large amount of memory.

We start by doing the taxonomic classification. The percentage identity treshold of 99% was used, to get a low type 1 error (reads must align more than 99%):

```bash
qiime feature-classifier classify-consensus-vsearch \
  --i-query ../data/rep-seqs.qza \
  --i-reference-reads ../data/silva_128_reads_99_unaligned.qza \
  --i-reference-taxonomy ../data/silva_128_tax_99_7l.qza \
  --p-perc-identity 0.99 \
  --o-classification ../data/taxonomy_vsearch_silva.qza \
  --p-threads 12 --verbose
```

Now we make the taxonomy table viewable:

```bash
qiime metadata tabulate \
      --m-input-file ../data/taxonomy_vsearch_silva.qza  \
      --o-visualization ../analysis/taxonomy_vsearch_silva.qzv
qiime tools view ../analysis/taxonomy_vsearch_silva.qzv
```

![taxa](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/taxa.png)

With this table we can check whether the taxonomic assignment can be verified by comparing it to the BLAST result of the [reference-sequence table](#seqtab) visualization of the respective feature ID. For example the second feature ID is classified as being part of the *Ruminococcaceae* family here. The BLAST result of the sequence confirm this and are shown below:

![BLAST](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/BLAST.png)



Next, we can generate an interactive visualization of the taxonomy with the following command:

```bash
qiime taxa barplot \
      --i-table ../data/table.qza \
      --i-taxonomy ../data/taxonomy_vsearch_silva.qza \
      --m-metadata-file ../data/metadata.tsv \
      --o-visualization ../analysis/taxa-bar-plots_silva1.qzv
qiime tools view ../analysis/taxa-bar-plots_silva1.qzv
```

![taxaplot_vsearch](/home/jakob/Documents/gut_diabetes/qiime2/analysis/pictures/taxaplot_vsearch.png)  

This shows the plot of phylae per sample. You can sort the samples by all features in the metadata inside the interactive plot. 







# Export ASV table

In order to do more complex downstream analysis, it is important to export the data into an OTU-table in the BIOM format. QIIME 2 has a plugin to do this easily:

```bash
qiime tools export \
      --input-path ../data/taxonomy_vsearch_silva.qza \
      --output-path ../../data/exported_bioms

qiime tools export \
      --input-path ../data/table.qza \
      --output-path ../../data/exported_bioms

cp ../../data/exported_bioms/taxonomy.tsv \
   ../../data/exported_bioms/biom-taxonomy_qiime.tsv

# it is important that you change the biom-tax file to a .txt file and change
# the header line to: #OTUID	taxonomy	confidence
# !!! 

biom add-metadata \
     -i ../../data/exported_bioms/feature-table.biom \
     -o ../../data/exported_bioms/table_w_taxonomy_qiime.biom \
     --observation-metadata-fp ../../data/exported_bioms/biom-taxonomy_qiime.txt \
     --sc-separated taxonomy
```

From this point on, we can do the further downstream analyses in R. We do this because for checking for differential abundance of taxa.























