#!/bin/bash

#folder specifies the folder, where fastqfiles are located. may differ!
java -Xms512m -Xmx4g -jar NGTax-2.1.14.jar -folder ../fastqfiles/ -for_p "GTGCCAGCMGCCGCGGTAA" -rev_p "GGACTACHVGGGTWTCTAAT" -refdb ../SILVA_128_SSURef_tax_silva.fasta.gz -b NGTax_diabetes1.biom -primersRemoved
