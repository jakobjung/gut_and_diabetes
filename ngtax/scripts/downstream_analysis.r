setwd("~/Documents/gut_diabetes/ngtax/scripts")
library(phyloseq)
library("ggplot2")
library("vegan")
library("DESeq2")
library(xlsx)
library(ape)
library("plyr")
theme_set(theme_bw())



bfile <- "../ngtax_result/result_ngtax.biom"
result <- import_biom(bfile, parseFunction = parse_taxonomy_default)

colnames(tax_table(result)) <- c("Domain", "Phylum", "Class", "Order", "Family", "Genus", "Species")

#change sample names of otus with regex:
sample_names(otu_table(result))
pat <- 'Sample\\d+_(.*?)_.*'
sample_names(otu_table(result)) <- gsub(pat,replace = '\\1', sample_names(otu_table(result)))
sample_names(otu_table(result))


#import metadata:
metadata <- read.csv('../../data/metadata/total_metadata.csv', header = TRUE)
rownames(metadata) <- metadata$X.SampleID
sample_data(result) <- metadata
head(sample_data(result))
# make data factorized:
sample_data(result)$months <- as.factor(sample_data(result)$months)
sample_data(result)$diet <- as.factor(sample_data(result)$diet)
sample_data(result)$debut.x <- as.factor(sample_data(result)$debut.x)
sample_data(result)$Clust <- as.factor(sample_data(result)$Clust)
sample_data(result)$Id.patient <- as.factor(sample_data(result)$Id.patient)
sample_data(result)$obesity <- as.factor(sample_data(result)$obesity)


#inspect results
rank_names(result)
result
#plot_bar(result, fill = "Domain") + theme(legend.position = "bottom")

# phylogenetic tree:
random_tree <- rtree(ntaxa(result), rooted = TRUE, tip.label = taxa_names(result))
#plot(random_tree)
phy_tree(result) <- random_tree

# create rarefraction courve:
#rarecurve(t(otu_table(result)), step=50, cex=0.5)
#plot_bar(result, fill="Rank2")

# plot richness:
# plot_richness(result)

# Standardize abundances to the median sequencing depth
total = median(sample_sums(result))
standf = function(x, t=total) round(t * (x / sum(x)))
res_stand <- transform_sample_counts(result, standf) # standardize
res_stand
#plot_bar(res_stand, fill = "Domain") + theme(legend.position = "bottom")

# Subset the data to Bacteroidetes:
bact <- subset_taxa(res_stand, Phylum=="p__Bacteroidetes")
title = "plot_bar; Bacteroidetes-only"
#plot_bar(bact, "diet_time", "Abundance", title=title)

# Subset the data to Firmicutes:
firm <- subset_taxa(res_stand, Phylum=="p__Firmicutes")
title = "plot_bar; Firmicutes-only"
#plot_bar(firm, "diet_time", "Abundance", title=title)

# Check alph diversity of debut diabetics comp to normals
start <- subset_samples(result, months==12)
#plot_richness(start, x = "debut.x", measures=c("Chao1", "Shannon")) 
# Check alph diversity between debut diabetics
diab <- subset_samples(result, debut.x==2)
#plot_richness(diab, x = "months", measures=c("Chao1", "Shannon")) 


# Distance:
dist_methods <- unlist(distanceMethodList) 
print(dist_methods) #check all options of distances
# Remove the user-defined distance
dist_methods = dist_methods[-which(dist_methods=="ANY")]
dist_methods = dist_methods[-which(dist_methods=="manhattan")]
#loop through all dist methods, store plots in list(plist)
plist <- vector("list", length(dist_methods))
names(plist) = dist_methods
for( i in dist_methods ){
  # Calculate distance matrix
  iDist <- phyloseq::distance(result, method=i)
  # Calculate ordination
  iMDS  <- ordinate(result, "MDS", distance=iDist)
  ## Make plot
  # Don't carry over previous plot (if error, p will be blank)
  p <- NULL
  # Create plot, store as temp variable, p
  p <- plot_ordination(result, iMDS, color="diet_time", shape = "debut.x")
  # Add title to each plot
  p <- p + ggtitle(paste("MDS using distance method ", i, sep=""))
  # Save the graphic to file.
  plist[[i]] = p
}


# Trees:
physeq = prune_taxa(taxa_names(result)[1:50], result) # arbitrarily prune to just the first 50 OTUs
#plot_tree(physeq,ladderize="left", label.tips="Genus") # dots are annotated otus in tree one for each sample otu was observed


# plot bars:
#plot_bar(result,"Phylum", facet_grid =~diet_time)

# diseq statistical analysis:
#create control and sardine vars:
control <- subset_samples(res_stand, diet==0)
sardine <- subset_samples(res_stand, diet==1)
# converse to diseq object:
c_diseq <- phyloseq_to_deseq2(control, ~ months)
s_diseq <- phyloseq_to_deseq2(sardine, ~ months)
c_diseq <- DESeq(c_diseq, test="Wald", fitType="parametric", sfType ="poscounts")
s_diseq <- DESeq(s_diseq, test="Wald", fitType="parametric", sfType ="poscounts")
# investigate results:
c_res <- results(c_diseq, cooksCutoff = FALSE)
s_res <- results(s_diseq, cooksCutoff = FALSE)
a <- 0.05
c_sigtab = c_res[which(c_res$padj < a), ]
s_sigtab = s_res[which(s_res$padj < a), ]
c_sigtab = cbind(as(c_sigtab, "data.frame"), as(tax_table(control)[rownames(c_sigtab), ], "matrix"))
s_sigtab = cbind(as(s_sigtab, "data.frame"), as(tax_table(sardine)[rownames(s_sigtab), ], "matrix"))
dim(s_sigtab)
head(c_sigtab)

#plot for sardine:
library("ggplot2")
theme_set(theme_bw())
scale_fill_discrete <- function(palname = "Set1", ...) {
  scale_fill_brewer(palette = palname, ...)
}
# Phylum order
x = tapply(s_sigtab$log2FoldChange, s_sigtab$Phylum, function(x) max(x))
x = sort(x, TRUE)
s_sigtab$Phylum = factor(as.character(s_sigtab$Phylum), levels=names(x))
# Genus order
x = tapply(s_sigtab$log2FoldChange, s_sigtab$Genus, function(x) max(x))
x = sort(x, TRUE)
s_sigtab$Genus = factor(as.character(s_sigtab$Genus), levels=names(x))
ggplot(s_sigtab, aes(x=Genus, y=log2FoldChange, color=Phylum)) + geom_point(size=6) + 
  theme(axis.text.x = element_text(angle = -90, hjust = 0, vjust=0.5))

#same for control
# Phylum order
x = tapply(c_sigtab$log2FoldChange, c_sigtab$Phylum, function(x) max(x))
x = sort(x, TRUE)
c_sigtab$Phylum = factor(as.character(c_sigtab$Phylum), levels=names(x))
# Genus order
x = tapply(c_sigtab$log2FoldChange, c_sigtab$Genus, function(x) max(x))
x = sort(x, TRUE)
c_sigtab$Genus = factor(as.character(c_sigtab$Genus), levels=names(x))
ggplot(c_sigtab, aes(x=Genus, y=log2FoldChange, color=Phylum)) + geom_point(size=6) + 
  theme(axis.text.x = element_text(angle = -90, hjust = 0, vjust=0.5))






















