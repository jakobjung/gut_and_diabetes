Title: Workflow NG-Tax 2.0 gut microbiota and diabetes

Author: Jakob Jung

Date: June 13, 2019



NG-Tax 2.0 is a pipeline for high throughput analysis of 16S rRNA amplicon sequences classification. It uses a *de novo* OTU-picking algorithm to generate Amplicon Sequence Variants (ASVs) instead of OTUs (similar to DADA2). 

# Initialization

Here, we explain in detail the NG-Tax workflow that we used in order to analyse the 16S rRNA data provided as FastQ-files. 

Java needs to be installed to run the code. It takes as input (-folder) our folder which contains all fastq-files of our demultiplexed reads. The primers used for the V4 region have to be specified (-for_p, rev_p).  The reference database (here we use SILVA) has to be specified for taxonomic classification and the output files are one .biom file and one RDF file in .ttl format. The -primersRemoved command is necessary because there are no primers left in our reads.

Because of problems with running ngtax in terms of memory on the laptop, we had to log into an external server to run it. Once in the server, we ran following code:

```bash
java -jar NGTax-2.1.14.jar \
  -folder ../fastqfiles/ \ 
  -for_p "GTGCCAGCMGCCGCGGTAA" \ 
  -rev_p "GGACTACHVGGGTWTCTAAT" \
  -refdb ../SILVA_128_SSURef_tax_silva.fasta.gz \
  -b fastq1.biom \
  -primersRemoved
```

NG-Tax 2.0 works as follows:

1. **Filtering:** 

   Forward and reverse reads are combined (not merged!). Readpairs with matching primers & barcodes are kept.

2. ***De novo*** **OTU picking:** 

   Per sample, amplicons are grouped into ASV objects by sequence identity. A minimal abundance threshold of 3 read pairs is used here. PCR / sequence specific errors, which could lead to wrong ASV objects, are inferred by 2 steps: 

   1. Using a read count treshold of 0.1% of total read counts in ASV objects, ASV objects below this are treated as follows: The program tries to merge those ASVs with other ASV objects with higher read counts of the same sample using a k-bounded Levenshtein function. It allows a cumulative edit distance of one nucleotide (mismatch/indel).  If it is unsuccessful, these ASVs are rejected, but still stored (most likely noise).
   2. Using the same strategy, remaining singleton read pairs are tried to be mergedwith the accepted set of ASVs also with a distance of 1 mismatch.

3. **Taxonomic assignment of ASVs:**

   We use the SILVA database as reference db. Reference sequences are trimmed (using the primers) to the V4 region to reduce computational costs. reference file is converted into a lookup table by clustering step. This table is then used for different samples. 

   A k-bounded Levenstein function with upper limit of 50 is used to find the distance between each ASV pair and entries in the reference file.  If the distance is below a mismatch of 15%, the frequency of occurrence in both reference db and tax annotation is generated and stored in the ASV object. This list is also stored in the biom file. 

   By some rules, the classifier afterwards proposes from the list of candidates the most likely taxonomic assignment (takes into account mismatches). depending on the mismatches, the taxonomy is assigned to a specific rank. E.g. between 100% -95% identity, the lowest taxonomic assignment is genus, between 95 and 92% it is family and below 92% it is order.

   The output files are a RDF file and a BIOM file, which can then be used for downstream analysis

An overview of the process can be found here:

![figure1](/home/jakob/Documents/gut_diabetes/literature/NG Tax 2.0/figure1.jpg)



Once we have the BIOM file, we can start downstream analysis in Rstudio

