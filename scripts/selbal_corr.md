

Title: Selbal & Correlations

Author: Jakob Jung

Date: December 4, 2019

# Overview

In this document we do zero replacement of the OTU-table and run the selbal algorithm in order to find the best ILR-balance of changes in bacterial genera that distinguishes people which returned to a healthy status after the study period from the ones that stay prediabetic or develop T2D. After that we use the balance to see whether it correlates with important T2D markers.

# Preparations

We import the packages that we use later:

```R
library(selbal)
library(zCompositions)
library(devtools)
library(xlsx)
library(dplyr)
library(rmcorr)
```

Now we import otu-table with all meta-data, add rownames and look at the first rows/columns of the dataframe:

```R
otu_with_all_meta <- read.xlsx(
"~/Documents/gut_diabetes/data/otu_genus_with_metadata.xlsx", 1)
rownames(otu_with_all_meta) <- otu_with_all_meta$NA.
otu_with_all_meta$NA. <- NULL
otu_with_all_meta[1:4,1:2]
```

|        |                  |                                                 |      |      |
| :----- | :--------------- | ----------------------------------------------: | ---- | ---- |
|        | Id.patient<fctr> | D_5__.Eubacterium..coprostanoligenes.group<dbl> |      |      |
| 101V0  | 101              |                                             869 |      |      |
| 101V12 | 101              |                                             633 |      |      |
| 102V0  | 102              |                                             506 |      |      |
| 102V12 | 102              |                                             272 |      |      |

And now we check all the names of the otu_table + metadata:



```R
colnames(otu_with_all_meta)
```

```
  [1] "Id.patient" 
  [2] "D_5__.Eubacterium..coprostanoligenes.group"
  [3] "D_5__.Eubacterium..eligens.group"           "D_5__.Eubacterium..hallii.group"
  [5] "D_5__.Eubacterium..ruminantium.group"       "D_5__.Eubacterium..ventriosum.group"
  [7] "D_5__.Eubacterium..xylanophilum.group"      "D_5__.Ruminococcus..gauvreauii.group"
  [9] "D_5__.Ruminococcus..torques.group"          "D_5__Akkermansia"
 [11] "D_5__Alistipes"                             "D_5__Anaerostipes"
 [13] "D_5__Bacteroides"                           "D_5__Barnesiella"
 [15] "D_5__Bifidobacterium"                       "D_5__Bilophila"
 [17] "D_5__Blautia"                               "D_5__Butyricicoccus"
 [19] "D_5__Butyricimonas"                         "D_5__Christensenellaceae.R.7.group"
 [21] "D_5__Clostridium.sensu.stricto.1"           "D_5__Collinsella"
 [23] "D_5__Coprobacter"                           "D_5__Coprococcus.1"
 [25] "D_5__Coprococcus.2"                         "D_5__Coprococcus.3"
 [27] "D_5__Defluviitaleaceae.UCG.011"             "D_5__Desulfovibrio"
 [29] "D_5__Dorea"                                 "D_5__Erysipelotrichaceae.UCG.003"
 [31] "D_5__Escherichia.Shigella"                  "D_5__Faecalibacterium"
 [33] "D_5__Family.XIII.AD3011.group"              "D_5__Family.XIII.UCG.001"
 [35] "D_5__Fusicatenibacter"                      "D_5__GCA.900066575"
 [37] "D_5__Haemophilus"                           "D_5__Holdemania"
 [39] "D_5__Lachnoclostridium"                     "D_5__Lachnospira"
 [41] "D_5__Lachnospiraceae.FCS020.group"          "D_5__Lachnospiraceae.NK4A136.group"
 [43] "D_5__Lachnospiraceae.UCG.001"               "D_5__Lachnospiraceae.UCG.004"
 [45] "D_5__Lachnospiraceae.UCG.010"               "D_5__Methanobrevibacter"
 [47] "D_5__Negativibacillus"                      "D_5__Odoribacter"
 [49] "D_5__Oscillibacter"                         "D_5__Parabacteroides"
 [51] "D_5__Paraprevotella"                        "D_5__Parasutterella"
 [53] "D_5__Peptococcus"                           "D_5__Phascolarctobacterium"
 [55] "D_5__Romboutsia"                            "D_5__Roseburia"
 [57] "D_5__Ruminiclostridium.5"                   "D_5__Ruminiclostridium.6"
 [59] "D_5__Ruminiclostridium.9"                   "D_5__Ruminococcaceae.NK4A214.group"
 [61] "D_5__Ruminococcaceae.UCG.002"               "D_5__Ruminococcaceae.UCG.003"
 [63] "D_5__Ruminococcaceae.UCG.005"               "D_5__Ruminococcaceae.UCG.010"
 [65] "D_5__Ruminococcaceae.UCG.013"               "D_5__Ruminococcaceae.UCG.014"
 [67] "D_5__Ruminococcus.1"                        "D_5__Ruminococcus.2"
 [69] "D_5__Streptococcus"                         "D_5__Subdoligranulum"
 [71] "D_5__Sutterella"                            "D_5__UBA1819"
 [73] "D_5__Veillonella"                           "D_5__Victivallis"
 [75] "unc_D_3__Mollicutes.RF39"                   "unc_D_4__Barnesiellaceae"
 [77] "unc_D_4__Christensenellaceae"                   						                [78]"unc_D_4__Clostridiales.vadinBB60.group"    
 [79] "unc_D_4__Flavobacteriaceae"                 "unc_D_4__Lachnospiraceae"
 [81] "unc_D_4__Muribaculaceae"                    "unc_D_4__Peptococcaceae"
 [83] "unc_D_4__Ruminococcaceae"                   "months"
 [85] "obesity"                                    "diet"
 [87] "variable._IMC"                              "debut"
 [89] "Sexe"                                       "Edat"
 [91] "Any.evolucio.GBA"                           "HTA"
 [93] "ECV"                                        "Dislipemia"
 [95] "AntiHTA"                                    "Antitrombòtics"
 [97] "HipoCOL"                                    "Protector.gàstric"
 [99] "Terapia.hormonal.tiroidea"                  "Antidepressius"
[101] "Broncodilatadors"                           "Analgesics.Opioides"
[103] "Altres.medicacions"                         "Tabaquisme"
[105] "Alcohol"                                    "Antecedents.Familiars.DM"
[107] "normalized_glycemia"                        "Sample_ID"
[109] "ID"                                         "Pes"
[111] "IMC"                                        "Cintura"
[113] "Cadera"                                     "ICC"
[115] "PAS"                                        "PAD"
[117] "FC"                                         "HbA1c"
[119] "Glucosa"                                    "Insulina"
[121] "Homa"                                       "glycemia.mml."                         
[123] "Homa.B"                                     "COL"                                   
[125] "HDL"                                        "LDL"                                   
[127] "TAG"                                        "AdipoQ"                               
[129] "time"                                       "FINRISC"   
```

We can see that columns 2 to 82 are bacterial genera. The rest is metadata.  



# Zero-patterns:
Firstly, we check how the zeros are distributed in the samples. We already deleted genera, which contained more than 50% zeros. There are 83 different genera included in :
```{r}
zPatterns(otu_with_all_meta[,1:83], label = 0,bar.labels = T)
```

![image-20191216120235507](/home/jakob/.config/Typora/typora-user-images/image-20191216120235507.png)





# Zero-replacement:

We do zero-replacement using the Geometric Bayesian multiplicative (GBM) method of the zCompositions package:

```R
X <- cmultRepl(otu_with_all_meta[,2:83], label = 0, method = "GBM", 
               output = c("prop","p-counts"),delta = 0.65, threshold = 0.5, 
               correct = TRUE, t = NULL, s = NULL, suppress.print = FALSE)
X[1:4,1]
```

|        |                                                 |      |
| :----- | ----------------------------------------------: | ---- |
|        | D_5__.Eubacterium..coprostanoligenes.group<dbl> |      |
| 101V0  |                                     0.015524611 |      |
| 101V12 |                                     0.006813485 |      |
| 102V0  |                                     0.008571320 |      |
| 102V12 |                                     0.003693926 |      |

264 values have been replaced. *Replaced* means that the initial imputing proportion value is lower than the minimum proportion observed and this value was *forced* to take the minimum observed proportion.

Other zero-replacements (e.g. SQ) could have been used, but this one showed the most promising results and fewest replacements.  



Now we merge the data again:

```R
otu_genus_gbm <- cbind(X, otu_with_all_meta[,84:length(otu_with_all_meta)])
```

# Changes:
We're interested in the changes of microbial abundances, since we have repeated measures.

 The permutations were created using following code:

```R
d=dim(X)
di=d[1]/2
PE<-as.data.frame(matrix(0,di,d[2]))
colnames(PE)=colnames(X)
j=1
for (i in 1:di){
  PE[i,]=(X[j+1,]/X[j,])/sum(X[j+1,]/X[j,])
  j=j+2
}
```

And we have to add the meta data again (continuous values (CV) represented as changes and categorical (AD) stay the same):

```R
AD <- as.data.frame(matrix(0,di,length(otu_genus_gbm[,84:106])))
CV <- as.data.frame(matrix(0,di,length(otu_genus_gbm[,109:length(otu_genus_gbm)])))

colnames(AD)=colnames(otu_genus_gbm[,84:106])
colnames(CV)=colnames(otu_genus_gbm[,109:length(otu_genus_gbm)])

j=1
for (i in 1:di){
  AD[i,] <- otu_genus_gbm[j,84:106]
  CV[i,] <- otu_genus_gbm[j+1,109:length(otu_genus_gbm)] - 
    otu_genus_gbm[j,109:length(otu_genus_gbm)] # differences
  j=j+2
}
CV$time <- NULL
AD$Edat <- NULL
AD[1:4,1:2]
```

|      | obesity<dbl> | diet<dbl> |
| :--- | -----------: | --------: |
| 1    |            1 |         1 |
| 2    |            2 |         1 |
| 3    |            1 |         1 |
| 4    |            2 |         1 |

Note that all categorical variables changed from 0/1 to 1/2 representing false/true, respectively!

```R
otu_genus_dif<-cbind(PE,AD)
```



# SELBAL

Now we can run selbal on the permutations, to find out which balance best explains the healthy group, using cross-validation. 

Here we run selbal which identifies the Genera (columns 1:82), which are able to predict the categorical variable of getting a normalized glycemia (column 104):

```R
Bal4 <- selbal.cv(otu_genus_dif[,1:82],as.factor(otu_genus_dif[,104]))
```

The optimal number of variables (genera) is: 6 



First we check the plot visualizing the performance of the selected balance (6 genera used) on all the data (without cv), and also showing which balance was selected :
```{r}
plot.new()
grid.draw(Bal4$global.plot)
```

![image-20191204091230947](/home/jakob/.config/Typora/typora-user-images/image-20191204091230947.png)

We can see that selbal put out six genera, which are included in the balance. 

The plot shows a quite high AUC of 0.831. However, this result is obtained using all data as test and training dataset. To see, how robust the result is, we look at the results of cross-validation:



We can show in the accuracy of CV per number of included genera:

```{r}
Bal4$accuracy.nvar
```

![image-20191204091849445](/home/jakob/.config/Typora/typora-user-images/image-20191204091849445.png)

Here, on the Y axis you can already see that the AUC values are far lower than in the previous plot. This is the result of cross-validation (testing data not included in training set), which shows that the resulting balance has moderate accuracy. 

Let's summarize the accuracy-data of cross-validation for our selected balance:
```{r}
Bal4$cv.accuracy
summary(Bal4$cv.accuracy)
```

```R
 [1] 0.7662338 0.7792208 0.5428571 0.5571429 0.5500000 0.6363636 0.5454545 0.5714286 0.5285714 0.7333333 0.4805195
[12] 0.5714286 0.6714286 0.4714286 0.6166667 0.5454545 0.5454545 0.6142857 0.6142857 0.4500000 0.6233766 0.6493506
[23] 0.4857143 0.6428571 0.5333333 0.6883117 0.5974026 0.4714286 0.6857143 0.6333333 0.7792208 0.5454545 0.5285714
[34] 0.5571429 0.5166667 0.5714286 0.6493506 0.5714286 0.4857143 0.7500000 0.5974026 0.6493506 0.5428571 0.5428571
[45] 0.6000000 0.6103896 0.6623377 0.8571429 0.7142857 0.5333333
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
 0.4500  0.5429  0.5844  0.6013  0.6494  0.8571 
```

We can conclude that the resulting balance performs well on the test dataset itself. However, when applying cross-validation, the accuracy is far lower (mean 0.6013 AUC), showing a lack of robustness.



Another output is the tabular plot showing information of Global Balance and the three most repeated balances in the cross - validation process:

```{r}
plot.new()
plot.tab(Bal4$cv.tab)
```

![image-20191204091502903](/home/jakob/.config/Typora/typora-user-images/image-20191204091502903.png)

And the barplot showing how often (%) each genus is included in the balances during CV procedure:

```{r}
Bal4$var.barplot
```

![image-20191204092047980](/home/jakob/.config/Typora/typora-user-images/image-20191204092047980.png)



We can conclude that the resulting balance performs well on the test dataset. However, when applying cross-validation, the accuracy is far lower, showing a lack of robustness. 



# Balance construction

Even though the CV-results are not very robust, we now use the balance to see whether it correlates with other factors such as Glucose and insulin levels. 
Firstly we need to create a vector for the balance for all test subjects. We use the ILR transformation as described in the selbal paper:
$$
B(X_+, X_-) = \sqrt{\frac{k_+ * k_-} {k_+ + k_-}} 
* \log \frac{(\prod _{i \varepsilon I_+}X_i)^{1/k_+}}{(\prod _{j \varepsilon I_-}X_j)^{1/k_-}}
$$
where X+ and X- are 2 subsets of components (numerator, denominator) indexed by I+ and I-, k+/- denounces the number of components, and the Balance B(X+,X-) is the normalized  log ratio of the geometric mean of the values for the two groups of components.

```{r}
s <- sqrt((3*3)/(3+3))
numerator <- (otu_genus_gbm$D_5__Barnesiella * 
                otu_genus_gbm$D_5__Defluviitaleaceae.UCG.011 *
                otu_genus_gbm$D_5__Ruminococcaceae.UCG.003)^(1/3)
denominator <- (otu_genus_gbm$D_5__UBA1819 * 
                  otu_genus_gbm$D_5__.Ruminococcus..torques.group *
                  otu_genus_gbm$D_5__Ruminiclostridium.5)^(1/3)
bal_selbal <- s * log(numerator/denominator)
otu_genus_gbm_with_selb <- cbind(otu_genus_gbm, bal_selbal)
dim(otu_genus_gbm)
dim(otu_genus_gbm_with_selb)
```
```
[1] 172 129
[1] 172 130
```

Now we have created the balance for the initial dataset (with both time 0 and 12). Now we can perform statistical tests on it. 



# Testing the selbal balance
Let's start by comparing the paired distances between the two time points for both the normalized glycemia and the other group(the ones that stayed prediabetic/turned diabetic). 

We first generate the 2 groups:
```{r}
norm <-  otu_genus_gbm_with_selb[(otu_genus_gbm_with_selb$normalized_glycemia == 1),]
diab <-  otu_genus_gbm_with_selb[(otu_genus_gbm_with_selb$normalized_glycemia == 0),]
```

We will first do some graphical comparison (see wheter balance can distinguish the 2 groups):
```{r}
par("mfrow"=c(1,2))
plot(norm$time, norm$bal_selbal, xlab = "time", ylab = "selbal-balance",
     main = "Turn Healthy")
plot(diab$time, diab$bal_selbal, xlab = "time", ylab = "selbal-balance",
     main = "Stay prediabetic")
```

![image-20191204093341401](/home/jakob/.config/Typora/typora-user-images/image-20191204093341401.png)

These 2 plots show that generally, for the people which turned healthy, the balance increased, whereas the balance of the others decreased (looking at differences in means). 

Now we do a wilcox test on the same data in a paired way, to see wheter there is indeed an increase for healthy and a decrease for diabetics:

```{r}
wilcox.test( norm[norm$time == 12,]$bal_selbal, norm[norm$time == 0,]$bal_selbal, 
             paired = T, alternative = "greater")
wilcox.test(diab[diab$time == 12,]$bal_selbal, diab[diab$time == 0,]$bal_selbal, 
            paired = T, alternative = "less")
```

```r
data:  norm[norm$time == 12, ]$bal_selbal and norm[norm$time == 0, ]$bal_selbal
V = 533, p-value = 6.979e-06
alternative hypothesis: true location shift is greater than 0


	Wilcoxon signed rank test with continuity correction

data:  diab[diab$time == 12, ]$bal_selbal and diab[diab$time == 0, ]$bal_selbal
V = 346, p-value = 0.000907
alternative hypothesis: true location shift is less than 0
```

Indeed, the assumptions are true, and the balance shows clear paired differences between the 2 groups. 



We also run a t-test to be sure that the results are significant:

```{r}
t.test( norm[norm$time == 12,]$bal_selbal, norm[norm$time == 0,]$bal_selbal, 
             paired = T, alternative = "greater")
t.test(diab[diab$time == 12,]$bal_selbal, diab[diab$time == 0,]$bal_selbal, 
            paired = T, alternative = "less")
```

```
	Paired t-test

data:  norm[norm$time == 12, ]$bal_selbal and norm[norm$time == 0, ]$bal_selbal
t = 5.054, df = 33, p-value = 7.868e-06
alternative hypothesis: true difference in means is greater than 0
95 percent confidence interval:
 1.075744      Inf
sample estimates:
mean of the differences 
               1.617316 


	Paired t-test

data:  diab[diab$time == 12, ]$bal_selbal and diab[diab$time == 0, ]$bal_selbal
t = -3.3474, df = 51, p-value = 0.0007692
alternative hypothesis: true difference in means is less than 0
95 percent confidence interval:
       -Inf -0.4622085
sample estimates:
mean of the differences 
             -0.9252862 
```

Indeed the t-test shows the same results.

# Repeated measures correlations

Now that we have validated the ability of the balance to differentiate between the 2 groups, let's check some correlations using repeated measures correlation (rmcorr), as described by Bakdash and Marusich (2017, doi: [10.3389/fpsyg.2017.00456](https://dx.doi.org/10.3389%2Ffpsyg.2017.00456)).

we start by checking pairwise correlations between the balance and Glucose values:

```{r}
gluc_rmcorr <- rmcorr(as.factor(ID), Glucosa, bal_selbal, otu_genus_gbm_with_selb)
plot(gluc_rmcorr)
```

![image-20191204142933351](/home/jakob/.config/Typora/typora-user-images/image-20191204142933351.png)

The plot suggests a negative correlation. 

Now we want to know the pvalue, correlarion coefficient and confidence intervals of this correlation. They are incorporated in the rmcorr object:

```R
gluc_rmcorr$p 
gluc_rmcorr$r
gluc_rmcorr$CI
```

```
[1] 0.0006512241
[1] -0.3645617
[1] -0.5389325 -0.1602309
```

The p-value (p = 0.0006512241) suggests significance and the negative coefficient (-0.3645617) shows a negative correlation, which can be derived from the plot as well. The glucose variable indicates fasted blood glucose (mg/dL). Patients with values above 125 are considered as diabetic and above 100 as prediabetic. 



Now we do the same tests for HbA1c:

```{r}
HbA1c_rmcorr <- rmcorr(as.factor(ID), HbA1c, bal_selbal, otu_genus_gbm_with_selb)
plot(HbA1c_rmcorr)
```

![image-20191204150129255](/home/jakob/.config/Typora/typora-user-images/image-20191204150129255.png)

```R
HbA1c_rmcorr$p
HbA1c_rmcorr$r
```

```
[1] 0.7373261
[1] -0.03736839
```

We cannot see any correlation here. This is interesting, because HbA1c is a measurement showing the averaged glucose levels over previous 2-3 months,  whereas the Glucose variable catches the current blood glucose levels, which are usually more variable. 



Let's try the same with FINRISC type 2 diabetes risk score, which takes into account age, BMI, waist circumference, elevated fasting blood glucose, use of blood pressure medication, dietary factors, genetic predispositions etc. :

```R
HbA1c_rmcorr <- rmcorr(as.factor(ID), FINRISC, bal_selbal, otu_genus_gbm_with_selb)
plot(HbA1c_rmcorr)
```

![image-20191204151157578](/home/jakob/.config/Typora/typora-user-images/image-20191204151157578.png)

```
HbA1c_rmcorr$p
HbA1c_rmcorr$r
HbA1c_rmcorr$CI
```

```
[1] 0.003954508
[1] -0.3076756
[1] -0.48976388 -0.09986809
```

Here we can again see a significant negative association between the balance and FINRISC score suggesting that the log ratios of genera in the balance might be connected to diabetes onset risk. This means that the balance which was used to discriminate people who returned to healthy status, can also explain a decrease in Diabetes risk (FINRISC).



# SELBAL for new variable

We wanted ro create a new variable, because, looking at the following graph, which shows normalized glycemia patients vs vs others, does have some unwanted properties :

![image-20191219091945335](/home/jakob/.config/Typora/typora-user-images/image-20191219091945335.png)

So we created a variable with only people who have strong changes in glucose (above 5 mg/dL):





Now we find balance for strong ones:

```{r}
otu_strong <- filter(otu_genus_dif, glucose_change %in% c(1,2))
Bal_change <- selbal.cv(otu_strong[,1:82],as.factor(otu_strong[,83]==1))
```



![image-20191219091742098](/home/jakob/.config/Typora/typora-user-images/image-20191219091742098.png)



```{r}
Bal_change$accuracy.nvar
```
![image-20191219092156515](/home/jakob/.config/Typora/typora-user-images/image-20191219092156515.png)



```{r}
Bal_change$var.barplot
```



![image-20191219092213148](/home/jakob/.config/Typora/typora-user-images/image-20191219092213148.png)

![image-20191219093753691](/home/jakob/.config/Typora/typora-user-images/image-20191219093753691.png)

