setwd("~/Documents/gut_diabetes/ngtax/scripts")
library(phyloseq)
library("ggplot2")
library("vegan")
library(xlsx)
library(ape)
library("plyr")
theme_set(theme_bw())
# find out paired distance with pldist:
library(pldist)



# for qiime2 result:
bfile_qiime <- "../../data/exported_bioms/table_w_taxonomy_qiime.biom"  
result <- import_biom(bfile_qiime, parseFunction = parse_taxonomy_default)
phy_tree(result) <- read_tree("../../data/exported_bioms/tree.nwk")

#import metadata:
metadata <- read.csv('../../data/metadata/total_metadata.csv', header = TRUE)
metadata$ID_n <- NULL

# add mormalized glycaemia patients metadata to sample_data:
glycemia <- read.xlsx('../../data/metadata/BBDD - UDG_nutrient_data.xlsx',4, header = TRUE)
glyc_healed <- glycemia[,-c(2:4)]
names(glyc_healed)[names(glyc_healed) == "normoGLC_V12"] <- "normalized_glycemia" #change colname
metadata <- merge.data.frame(metadata, glyc_healed, by = "Id.patient", all.x = T)
rownames(metadata) <- metadata$X.SampleID

sample_data(result) <- metadata
head(sample_data(result))

# make data factorized:
sample_data(result)$months <- as.factor(sample_data(result)$months)
sample_data(result)$diet <- as.factor(sample_data(result)$diet)
sample_data(result)$debut.x <- as.factor(sample_data(result)$debut.x)
sample_data(result)$Clust <- as.factor(sample_data(result)$Clust)
sample_data(result)$Id.patient <- as.factor(sample_data(result)$Id.patient)
sample_data(result)$obesity <- as.factor(sample_data(result)$obesity)
sample_data(result)$normalized_glycemia <- as.factor(sample_data(result)$normalized_glycemia)

##pldist:


# rename cols:
sample_data(result)$subjID <- sample_data(result)$Id.patient
sample_data(result)$sampID <- sample_data(result)$X.SampleID
sample_data(result)$time <- sample_data(result)$months
names(sample_data(result))

# create physeq object for only sardine and control separated :
sard <- subset_samples(result, diet == 1) 
cont <- subset_samples(result, diet == 0) 
dim(otu_table(sard))
dim(otu_table(cont))

# put OTU and data into right format to perform pldist:
sard_pd_meta <- sample_data(sard)[,8:10]
cont_pd_meta <- sample_data(cont)[,8:10]
tot_pd_meta <- sample_data(result)[,8:10]

sard_pd_otu <- t(otu_table(sard))
cont_pd_otu <- t(otu_table(cont))
tot_pd_otu <- t(otu_table(result))



# run pldist data preparation with the data:
sard_data_pl <- data_prep(sard_pd_otu, sard_pd_meta, paired = T) 
cont_data_pl <- data_prep(cont_pd_otu, cont_pd_meta, paired = T) 
tot_data_pl <- data_prep(tot_pd_otu, tot_pd_meta, paired = T)

# transform in a paired way:
res_sard <- pltransform(sard_data_pl, paired = T, norm = T) # not sure with norm = T. It normalizes by overall abundance...
res_cont <- pltransform(cont_data_pl, paired = T, norm = T)
res_tot <- pltransform(tot_data_pl, paired = T, norm = T)

## UniFrac Family Distances:
# first, we need a tree:
sard_tree <-  phy_tree(sard) 
cont_tree <- phy_tree(cont) 
tot_tree <- phy_tree(result) 


sard_unifrac <- clr_LUniFrac(otu.tab = sard_pd_otu, metadata = sard_pd_meta, tree = sard_tree, 
                             gam = c(0, 0.5, 1), paired = TRUE)#, check.input = TRUE)

sard_pldist <- pldist(sard_pd_otu, sard_pd_meta, paired = TRUE, binary = FALSE, clr = TRUE, method = "gower")
sard_pldist$D[1:5,1:5]

tot_pldist <- pldist(tot_pd_otu, tot_pd_meta, paired = TRUE, binary = FALSE, clr = TRUE, method = "braycurtis")

groups <- rownames(tot_pldist$D) %in% sample_data(sard)$subjID
groups <- ifelse(groups,"SG", "CG")
groups

sd <- dist(tot_pldist$D, method = "euclidean")
bd<-betadisper(sd, groups)

par(mfrow=c(1,1))
plot(bd)
boxplot(bd)
anova(bd)





