# Here I will create a pot of diabetes prevalence per age-group in europe and spain:

#clear global environment:
rm(list=ls())

#get packages:
library(xlsx)
library(ggplot2)
library(dplyr)

# set working directory:
setwd("~/Documents/gut_diabetes/scripts/")

# import global diabetes data for diabetes prevalence per country etc. :
glob_data <- read.xlsx("../data/Diabetes_worldwide/Diabetes_per_age.xlsx",1)

# get data only for europe:
euro_data <- glob_data[glob_data$IDF.region=="EUR",4:8]

# delete columns that are irrelevant:
euro_data$Report.Gender <- NULL
euro_data$Report.set <- NULL

# Inspect eur_data:
dim(euro_data)

# get the data for whole europe by age classes:
euro_data %>% 
  group_by(Report.Age) %>%
  summarise_all(funs(sum)) -> eur

# create new variable with percentage:
eur$perc_diabetes <- (eur$Adults.with.diabetes..20.79..in.1.000s/eur$Adults.population..20.79..in.1.000s)*
  100




# get data only from spain:
spain_data <- glob_data[glob_data$report_country=="Spain",4:8]

# delete columns that are irrelevant:
spain_data$Report.Gender <- NULL
spain_data$Report.set <- NULL

# Inspect spain_data:
dim(spain_data)

# get the data for whole spainpe by age classes:
spain_data %>% 
  group_by(Report.Age) %>%
  summarise_all(funs(sum)) -> spain

# create new variable with percentage:
spain$perc_diabetes <- (spain$Adults.with.diabetes..20.79..in.1.000s/spain$Adults.population..20.79..in.1.000s)*
  100

dim(spain)
dim(eur)

merge(eur,spain, by = )


# create plot for percentage of diabetes per age group:
ggplot(data = eur, mapping = aes(x = Report.Age, y = perc_diabetes)) + geom_point() + 
  labs(x = "Age range", y = "Prevalence diabetes (in %)") 

ggplot(data = spain, mapping = aes(x = Report.Age, y = perc_diabetes)) + geom_point() + 
  labs(x = "Age range", y = "Prevalence diabetes (in %)") 


ggplot() + geom_line(data=eur, aes(x = Report.Age, y = perc_diabetes, group = 1, color="Europe"),size=1.5) +
  geom_line(data=spain, aes(x = Report.Age, y = perc_diabetes, group = 1, color="Spain"),size=1.5) +
  geom_point(data=eur, aes(x = Report.Age, y = perc_diabetes, group = 1, color="Europe"),size=4) +
  geom_point(data=spain, aes(x = Report.Age, y = perc_diabetes, group = 1, color="Spain"),size=4) + 
  scale_color_manual(values = c('Spain' = 'coral2', 'Europe' = 'darkblue')) +
  labs(x = "Age range", y = "Diabetes prevalence (in %)") +
  theme(text=element_text(size=10,family="Serif"))+
  theme(
    # Change plot and panel background
    # Change legend 
    axis.title = element_text(size = 20),
    axis.text = element_text(size = 15),
    legend.text = element_text(size = 15),
    legend.title = element_blank()
  )

  




