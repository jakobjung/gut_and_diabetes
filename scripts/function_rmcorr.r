#function to get p-value of rmcorr:
get_pval<- function(participant, measure1, measure2, dataset){ 
  rmcorr(participant, measure1, measure2, dataset)$p
}

# function to get coefficient of rmcorr:
get_corrco<- function(participant, measure1, measure2, dataset){ 
  rmcorr(participant, measure1, measure2, dataset)$r
}

# inputs: otu must have otus as colnames and sample IDs as rownames. 
#         test_table must have same rownames and first col as "ID" of patient
do_correlations <- function(otu, test_table) {
  #get colnames of otus:
  names_otu <- colnames(otu)
  names_test <- colnames(test_table[,2:length(test_table)])
  
  # merge the 2 dfs:
  merged <- merge(otu, test_table, by = 0)
  merged$ID <- as.factor(merged$ID)
  
  # create 2 empty datarames which are wanted as result:
  pvals <- data.frame(matrix(nrow = length(names_otu)), row.names = names_otu)
  corcoeffs <- data.frame(matrix(nrow = length(names_otu)), row.names = names_otu)
  
  # run rmcorr between all otus and all test variables:
  for (fac in names_test){
    p <- list()
    r <- list()
    for (bact in names_otu) {
      p[[bact]] <- get_pval('ID', bact, fac, merged)
      r[[bact]] <- get_corrco('ID', bact, fac, merged)
    }
    pvals[[fac]] <- unlist(p)
    corcoeffs[[fac]] <- unlist(r)
  }
  pvals[,1] <- NULL
  corcoeffs[,1] <- NULL
  return(list(coefficients = corcoeffs, pvalues = pvals))
}

x <- data.frame(matrix(ncol = 3, nrow = 0))
x <- list()

for (fac in names(clin_data[,3:length(clin_data)])){
  for (i in names_otu) {
    x[[i]] <- get_pval('ID', i, fac, merged_clin_micr_data)
  }
  cors <- unlist(x)
  pvals[[fac]] <- cors
}


corr_gluc <- data.frame(matrix(unlist(x), nrow=length(x), byrow=T))
rownames(corr_gluc) <- names(x)
