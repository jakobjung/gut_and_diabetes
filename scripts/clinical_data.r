setwd("/home/jakob/Documents/gut_diabetes/scripts")
library(xlsx)
library("ggpubr")

# import clinical data of patients at time 0 and 12:
data_V0 <-  as.data.frame(read.xlsx('../data/metadata/bbdd_girona_baseline_V0+V12.xlsx', 1, header = TRUE))
data_V0 <-data_V0[1:186,1:21]
data_V12 <- as.data.frame(read.xlsx('../data/metadata/bbdd_girona_baseline_V0+V12.xlsx', 2, header = TRUE))
data_V12 <-data_V12[1:125,]
data_V12 <- data_V12[-which(rowMeans(is.na(data_V12)) > 0.7),]


# check dataframes:
dim(data_V0)
dim(data_V12)
head(data_V0)
head(data_V12)

#change IDs:
data_V0$Sample_ID <-  sapply(as.character(data_V0$ID), paste, "V0", sep="")
data_V12$Sample_ID <-  sapply(as.character(data_V12$ID), paste, "V12", sep="")
data_V0$Sample_ID
data_V12$Sample_ID

# add column for time point:
data_V0$time <- as.factor(rep_len(0,length(data_V0$ID)))
data_V12$time <- as.factor(rep_len(12,length(data_V12$ID)))

# delete samples of V0 that are not measured in V12 -> dfs of same length:
data_V0 <- data_V0[(data_V0$ID %in% data_V12$ID),]
dim(data_V0)
dim(data_V12)

# change rownames to sample id
rownames(data_V0) <- data_V0$Sample_ID
rownames(data_V12) <- data_V12$Sample_ID

#merge the 2 dataframes:
clin_data <- rbind(data_V0, data_V12)
dim(clin_data)
head(clin_data)
clin_data <- clin_data[order(row.names(clin_data)), ]
clin_data$AdipoQ

# add some metadata (diet etc.):
whole_metadata <- read.xlsx('../data/metadata/mapping_file.xlsx', 1, header = TRUE)
whole_metadata <- as.data.frame(whole_metadata)
colnames(whole_metadata)[which(names(whole_metadata) == "X.SampleID")] <- "Sample_ID"
whole_metadata$BarcodeSequence <- NULL
whole_metadata$LinkerPrimerSequence <- NULL
whole_metadata$Sample_ID <- gsub("O", "0", whole_metadata$Sample_ID) # Change 0 for O

clin_data <- merge(clin_data, whole_metadata, by='Sample_ID')

#factorize factor columns:
clin_data[,22:43] <- lapply(clin_data[,22:43], as.factor) 
clin_data[,1] <- as.factor(clin_data[,1])

#split
sardin <- clin_data[clin_data$diet == 1,]
control <- clin_data[clin_data$diet == 0,]


par(mfrow=c(1,2)) 
boxplot(control[control$time == 0,]$Glucosa,control[control$time == 12,]$Glucosa, main="CG",
        xlab="Months", ylab="fasting glucose level in mg/dL", names=c("0","12"))
boxplot(sardin[sardin$time == 0,]$Glucosa,sardin[sardin$time == 12,]$Glucosa, main="SG",
        xlab="Months", ylab="fasting glucose level in mg/dL", names=c("0","12"))


par(mfrow=c(1,2)) 
ggdensity(clin_data$Glucosa, main = "Density plot glucose",xlab = "glucose level in mg/dL")
ggqqplot(clin_data$Glucosa)  #points far from line  if not normally distributed
shapiro.test(clin_data$Glucosa) #p value above 0.05 if not normally distributed


#check for changes in glucose in control and sardin group:
t.test(sardin[sardin$time == 0,]$Glucosa,sardin[sardin$time == 12,]$Glucosa, 
       alternative = "two.sided", paired = T, conf.level = 0.99)
t.test(control[control$time == 0,]$Glucosa,control[control$time == 12,]$Glucosa, 
       alternative = "two.sided", paired = T, conf.level = 0.99)

#check for changes in adipoQ in control and sardin group:
par(mfrow=c(1,2)) 
boxplot(control[control$time == 0,]$AdipoQ,control[control$time == 12,]$AdipoQ, main="CG",
        xlab="Months", ylab="Adipoq level", names=c("0","12"))
boxplot(sardin[sardin$time == 0,]$AdipoQ,sardin[sardin$time == 12,]$AdipoQ, main="SG",
        xlab="Months", ylab="Adipoq level", names=c("0","12"))

# get only the important clinical data:
s1 <- sardin[,3:23]
c1 <- control[,3:23]

#create dfs before and after:
s_bef <- s1[s1$time==0,]
s_aft <- s1[s1$time==12,]
c_bef <- c1[c1$time==0,]
c_aft <- c1[c1$time==12,]

# run t-tests for all selected variables, safe as dataframe:
sard_t_tests <- sapply(colnames(sardin[,3:22]), function(x) {
  t.test(s_bef[[x]], s_aft[[x]], paired = T, alternative = "two.sided", conf.level = 0.99)})
sard_t_tests <- as.data.frame(t(sard_t_tests))

# same for control:
cont_t_tests <- sapply(colnames(control[,3:22]), function(x) {
  t.test(c_bef[[x]], c_aft[[x]], paired = T, alternative = "two.sided", conf.level = 0.99)})
cont_t_tests <- as.data.frame(t(cont_t_tests))

dim(sard_t_tests)
dim(cont_t_tests)

sign_ch <- sard_t_tests[sard_t_tests$p.value<0.05 & cont_t_tests$p.value>0.05,]
sign_ch

library(naivebayes)
library(ggplot2)
library("dplyr")
library(psych)

colnames(data_V12)
changes_glc <- cbind(data_V12[2:21] - data_V0[2:21], data_V12$Sample_ID) 
colnames(changes_glc)[which(names(changes_glc) == "data_V12$Sample_ID")] <- "Sample_ID"
changes_glc<- merge(changes_glc, whole_metadata, by='Sample_ID') # get the changes of blood data
changes_glc[,22:26] <- lapply(changes_glc[,22:26], as.factor)
changes_glc$Alçada <- NULL
changes_glc$months <- NULL
changes_glc$LDL <- NULL
changes_glc$TAG <- NULL
changes_glc$glycemia.mml. <- NULL
changes_glc$Insulina <- NULL
changes_glc$Pes <-NULL
changes_glc$obesity <- NULL
changes_glc$debut <- NULL
changes_glc$Id.patient <- NULL
changes_glc$Sample_ID <- NULL

names(changes_glc)
pairs.panels(changes_glc[,2:15])
names(changes_glc)

changes_glc %>% 
  ggplot(aes(x=diet, y=Glucosa, fill = diet)) + geom_boxplot() + ggtitle("Box Plot")

changes_glc %>% 
  ggplot(aes(x=Homa.B, fill = diet)) + geom_density(alpha=0.8, color="black") + ggtitle("Dens Plot")
  
# Naive bayes model:
nb <- naive_bayes(diet ~ ., data = changes_glc)
nb
plot(nb)

p <- predict(nb, changes_glc, type = "prob")
cbind(p, changes_glc$diet)

p1 <- predict(nb,changes_glc)
(tab1 <- table(p1,changes_glc$diet))
 # not really effective! 






