#!/bin/bash

# create new directories, if they don't exist yet
mkdir -p ../../data/quality_check_fastq
mkdir -p ../../analysis/quality_check_fastq

# pool all forward and reversew reads sep. to assess quality
if [[ ! -e ../../data/quality_check_fastq/forward_reads.fastq ]]; then
cat ../../data/fastqfiles/*_R1*.fastq >> ../../data/quality_check_fastq/forward_reads.fastq
cat ../../data/fastqfiles/*_R2*.fastq >> ../../data/quality_check_fastq/reverse_reads.fastq
fi

# run fastqc, store htmls in analysis folder
fastqc ../../data/quality_check_fastq/forward_reads.fastq -o ../../analysis/quality_check_fastq/
fastqc ../../data/quality_check_fastq/reverse_reads.fastq -o ../../analysis/quality_check_fastq/


