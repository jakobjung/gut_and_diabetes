---
title: "Analysis of blood data from sardin study"
author: "Jakob Jung"
date: "August 5, 2019"
output:
  pdf_document: default
  word_document: default
  html_document: default
---

In the folowing R-script I analyzed the paired changes (2 time points per person) of 

# Data preparation
Here I start by downloading the clinical data and find out some effects of diet on this data.

## Initialization
I start with setting the working directory and importing the most important packages:
```{r setup}
setwd("/home/jakob/Documents/gut_diabetes/scripts")
library(xlsx)
library("ggpubr")
```

## Data preparation:
Here I import clinical data of patients at time 0 and 12 and delete some samples which have a high amount of missing values (NA), showing that at time 12 these were not measured:
```{r}
data_V0 <-  as.data.frame(read.xlsx('../data/metadata/bbdd_girona_baseline_V0+V12.xlsx', 
                                    1, header = TRUE)) # Get data from timepoint 0
data_V0 <-data_V0[1:186,1:21]  # get rid of empty cols and rows
data_V12 <- as.data.frame(read.xlsx('../data/metadata/bbdd_girona_baseline_V0+V12.xlsx', 
                                    2, header = TRUE)) # Get data from timepoint 12
data_V12 <-data_V12[1:125,]  # get rid of empty rows
data_V12 <- data_V12[-which(rowMeans(is.na(data_V12)) > 0.7),] # get rid of samples with high amount of NA's (more than 70%)
```

Now I check for the dataframe's properties and attributes:
```{r}
dim(data_V0)
dim(data_V12)
head(data_V0)[1:5,1:5]
head(data_V12)[1:5,1:5]
summary(data_V12)
```
You can see that there are still some NA values in most of the columns, which we have to keep in mind for statistical analyses.
Now I change the IDs to give each row an individual ID when merging the dataframes:
```{r}
data_V0$Sample_ID <-  sapply(as.character(data_V0$ID), paste, "V0", sep="")
data_V12$Sample_ID <-  sapply(as.character(data_V12$ID), paste, "V12", sep="")
data_V0$Sample_ID[1:5]
data_V12$Sample_ID[1:5]
```

Now I add another column for the time point in both dataframes:
```{r}
data_V0$time <- as.factor(rep_len(0,length(data_V0$ID)))
data_V12$time <- as.factor(rep_len(12,length(data_V12$ID)))
```

And I delete all samples in the dataframe of time 0, which are not included in time 12. So I get 2 dataframes of same size, with the same sample IDs at 2 different time points:
```{r}
data_V0 <- data_V0[(data_V0$ID %in% data_V12$ID),]
dim(data_V0)
dim(data_V12)
```

Now I change the rownames to Sample ID:
```{r}
# change rownames to sample id
rownames(data_V0) <- data_V0$Sample_ID
rownames(data_V12) <- data_V12$Sample_ID
dim(data_V12)
```

And I merge the 2 dataframes:
```{r}
clin_data <- rbind(data_V0, data_V12)
dim(clin_data)
clin_data[1:5,1:5]
clin_data <- clin_data[order(row.names(clin_data)), ] # order by rownames

```
Now let's add some metadata (diet,etc) to our data:
```{r}
whole_metadata <- read.xlsx('../data/metadata/mapping_file.xlsx', 1, header = TRUE)
whole_metadata <- as.data.frame(whole_metadata)
colnames(whole_metadata)[which(names(whole_metadata) == "X.SampleID")] <- "Sample_ID"
whole_metadata$BarcodeSequence <- NULL
whole_metadata$LinkerPrimerSequence <- NULL
whole_metadata$Sample_ID <- gsub("O", "0", whole_metadata$Sample_ID) # Change 0 for O

clin_data <- merge(clin_data, whole_metadata, by='Sample_ID')

#factorize factor columns:
clin_data[,23:28] <- lapply(clin_data[,23:28], as.factor) 
clin_data[,1] <- as.factor(clin_data[,1])

#save clinical data:
write.csv(clin_data, "../data/metadata/clinical_data_blood.csv")
```

And split the df into the sardin group and control group:
```{r}
sardin <- clin_data[clin_data$diet == 1,]
control <- clin_data[clin_data$diet == 0,]
```

Now we have one dataframe with all data, one just for the sardine group (SG) and one just for the control group (CG). With these I can run some statistical tests.

## Statistical tests
Now I want to find out whether the levels of some blood levels changed over time for the sardine group. This needs to be done in a paired way to take the differences per sample (subjects measured twice) into account.

I start with a comparison in blood glucose levels between before and after 12 months of the study for both control group (CG) and sardine group (SG).

I make a boxplot to see how the medians and quartiles are changing during the study:
```{r}
par(mfrow=c(1,2)) 
boxplot(control[control$time == 0,]$Glucosa,control[control$time == 12,]$Glucosa, main="CG",
        xlab="Months", ylab="fasting glucose level in mg/dL", names=c("0","12"))
boxplot(sardin[sardin$time == 0,]$Glucosa,sardin[sardin$time == 12,]$Glucosa, main="SG",
        xlab="Months", ylab="fasting glucose level in mg/dL", names=c("0","12"))
```

This shows a slight decrease in median glucose level. Interestingly, the median glucose level for the sardine group looks to decrease, whereas the boxplot of the control group is not changing much. This might suggest a negative correleation between fasting glucose and sardine diet. To see whether these differences are significant, we have to perform a paired test. A paired t-test is the preferred method when the data is assumed to be normally distributed. We can easily check for normality with a density plot for glucose in the whole dataset:
```{r}
par(mfrow=c(1,2)) 
ggdensity(clin_data$Glucosa, main = "Density plot glucose",xlab = "glucose level in mg/dL")
ggqqplot(clin_data$Glucosa)  #points far from line  if not normally distributed
shapiro.test(clin_data$Glucosa) #p value above 0.05 if not normally distributed
```

Looking at the density plot, the QQ-plot and the Shapiro-Wilk normality test, the levels of glucose seem to be rather normally distributed. Therefore I decided to run a paired t-test. If the data was not normally distributed, I would have switched to a Wilcoxon-signed rank test instead. 

Here I perform the paired t-test: 
```{r}
t.test(sardin[sardin$time == 0,]$Glucosa,sardin[sardin$time == 12,]$Glucosa, 
       alternative = "two.sided", paired = T, conf.level = 0.99)
t.test(control[control$time == 0,]$Glucosa,control[control$time == 12,]$Glucosa, 
       alternative = "two.sided", paired = T, conf.level = 0.99)
```

The paired t-tests show a significant decrease in blood glucose level for the sardin group and not for the control group. I can therefore conclude that the sardin diet has effected in a lower blood glucose level. 

Let's do the same analysis for Adiponectin, a protein hormrne related to glucose regulation. 
I start by making a boxplot again:
```{r}
par(mfrow=c(1,2)) 
boxplot(control[control$time == 0,]$AdipoQ,control[control$time == 12,]$AdipoQ, main="CG",
        xlab="Months", ylab="Adipoq level", names=c("0","12"))
boxplot(sardin[sardin$time == 0,]$AdipoQ,sardin[sardin$time == 12,]$AdipoQ, main="SG",
        xlab="Months", ylab="Adipoq level", names=c("0","12"))
```

We can see a positive effect in both control and sardine group. To check for significance, we again run a paired t-test:
```{r}
t.test(sardin[sardin$time == 0,]$AdipoQ,sardin[sardin$time == 12,]$AdipoQ, 
       alternative = "two.sided", paired = T, conf.level = 0.99)
t.test(control[control$time == 0,]$AdipoQ,control[control$time == 12,]$AdipoQ, 
       alternative = "two.sided", paired = T, conf.level = 0.99)
```
Interestingly, The changes in the control group are not significant (pval 0.9) whereas in the sardine group we find significance (pval 0.0014). It looks like the sardine diet has led to an extra effect to increase adiponectin levels.

Because there are more variables than just glucose and adipoQ, I will run the paired t-test on all of the variales of clinical data:
```{r}
# get only the important clinical data:
s1 <- sardin[,3:23]
c1 <- control[,3:23]

#create dfs before and after:
s_bef <- s1[s1$time==0,]
s_aft <- s1[s1$time==12,]
c_bef <- c1[c1$time==0,]
c_aft <- c1[c1$time==12,]

# run t-tests for all selected variables, safe as dataframe:
sard_t_tests <- sapply(colnames(sardin[,3:22]), function(x) {
  t.test(s_bef[[x]], s_aft[[x]], paired = T, alternative = "two.sided", conf.level = 0.99)})
sard_t_tests <- as.data.frame(t(sard_t_tests))

# same for control:
cont_t_tests <- sapply(colnames(control[,3:22]), function(x) {
  t.test(c_bef[[x]], c_aft[[x]], paired = T, alternative = "two.sided", conf.level = 0.99)})
cont_t_tests <- as.data.frame(t(cont_t_tests))

dim(sard_t_tests)
dim(cont_t_tests)
```

I created 2 dataframes (control, sardin) of the same length with rows for variables and columns for the numbers of the t-test for each variable (e.g. p-value).

Now I want to check, which of the tests shows significant changes in the SG while no changes in the CG. This would make it possible to conclude an effect of sardine:

```{r}
sign_ch <- sard_t_tests[sard_t_tests$p.value<0.05 & cont_t_tests$p.value>0.05,]
sign_ch
```

Here you can see that Glucose, Hip-circ and AdipoQ (we already knew that from previous analyses) are changing significantly in the SG and not in the CG.  This is interesting and can be noted and compared to literature.
















