# set the working directory to the folder it's in:
setwd("/home/jakob/Documents/gut_diabetes/scripts")

# import the metadata (whole metadata):
library(xlsx)
whole_metadata <- read.xlsx('../data/metadata/all_metadata_no_row1.xlsx', 1, header = TRUE)
dim(metadata)

# import the qiime metadata sheet:
qiime_metadata <- read.xlsx('../data/metadata/mapping_file.xlsx', 1, header = TRUE)
head(whole_metadata)

#modify sheet (get rid of na's):
whole_metadata <- whole_metadata[1:(length(whole_metadata)-5)] # delete last cols.
dim(whole_metadata)
n <- dim(whole_metadata)[1]  #get length
whole_metadata <- whole_metadata[1:(n-7),] #delete last 7 rows (NA's)

# create new col in qime metadata showing diet and time of taking:
qiime_metadata$X.SampleID
qiime_metadata$diet_time <- "nodiet_0"
qiime_metadata$diet_time[qiime_metadata$diet == 1 & qiime_metadata$months == 12] <- "sardin_12"
qiime_metadata$diet_time[qiime_metadata$diet == 1 & qiime_metadata$months == 0] <- "sardin_0"
qiime_metadata$diet_time[qiime_metadata$diet == 0 & qiime_metadata$months == 12] <- "nodiet_12"
qiime_metadata$diet_time[qiime_metadata$diet == 0 & qiime_metadata$months == 0] <- "nodiet_0"

# make it a factor (for categorical data)
qiime_metadata$diet_time <- as.factor(qiime_metadata$diet_time)

#save mapping file
write.csv(qiime_metadata, '../data/metadata/mapping_file.csv')

expr_id <- '(\\d*)(V.*)'
qiime_metadata$ID <- gsub(expr_id, replace = '\\1', qiime_metadata$X.SampleID)
qiime_metadata$ID
qiime_metadata$X.SampleID
whole_metadata$ID

total <- merge(qiime_metadata, whole_metadata, by='ID')
total$ID <- NULL # delete ID col
total$BarcodeSequence <- NULL
total$LinkerPrimerSequence <- NULL


write.csv(total, '../data/metadata/total_metadata.csv', row.names = FALSE)






