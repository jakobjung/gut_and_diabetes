# selbal tries:
# import selbal and other packages
library(selbal)
library(zCompositions)
library(devtools)
library(xlsx)
library(rmcorr)

# Import otu-table with all metadata:
otu_with_all_meta <- read.xlsx("Documents/gut_diabetes/data/otu_genus_with_metadata.xlsx", 1)
rownames(otu_with_all_meta) <- otu_with_all_meta$NA.
otu_with_all_meta$NA. <- NULL

# Check the zero patterns:
zPatterns(otu_with_all_meta[,2:83],label=0, cell.labels=c("Zero","Non-zero"), bar.labels=TRUE)

# Do bayesian zero replacement:
X<-cmultRepl(otu_with_all_meta[,2:83], label = 0, method = "GBM", output = c("prop","p-counts"),
             delta = 0.65, threshold = 0.5, correct = TRUE, t = NULL, s = NULL,
             suppress.print = FALSE)

# merge again:
otu_genus_gbm<-cbind(X, otu_with_all_meta[,84:length(otu_with_all_meta)])

#Compute the CoDa difference between V0 and V12 using perturbation. We use X, without metadata. 
#We use the same column names
d=dim(X)
di=d[1]/2
PE<-as.data.frame(matrix(0,di,d[2]))
colnames(PE)=colnames(X)
j=1
for (i in 1:di){
  PE[i,]=(X[j+1,]/X[j,])/sum(X[j+1,]/X[j,])
  j=j+2
}

#Add the metadata columns
#Warning: we have to add only a row for each patient
AD <- as.data.frame(matrix(0,di,length(otu_genus_gbm[,84:106])))
CV <- as.data.frame(matrix(0,di,length(otu_genus_gbm[,109:length(otu_genus_gbm)])))

colnames(AD)=colnames(otu_genus_gbm[,84:106])
colnames(CV)=colnames(otu_genus_gbm[,109:length(otu_genus_gbm)])

j=1
for (i in 1:di){
  AD[i,] = otu_genus_gbm[j,84:106]
  CV[i,] = otu_genus_gbm[j+1,109:length(otu_genus_gbm)] - otu_genus_gbm[j,109:length(otu_genus_gbm)] # differences
  j=j+2
}
AD
otu_genus_dif<-cbind(PE,AD,CV)

# do selbal without differences at both times:
Bal <- selbal(otu_genus_gbm[,1:82],as.factor(otu_genus_gbm[,105]))
Bal <- selbal.cv(otu_genus_gbm[,1:82],as.factor(otu_genus_gbm[,105]))
Bal$accuracy.nvar
Bal$var.barplot
plot.new()
grid.draw(Bal$global.plot)
plot.new()
plot.tab(Bal$cv.tab)
Bal$cv.accuracy; summary(Bal$cv.accuracy)


# Apply SELBAL now on the differences (people who turned healthy vs others):
Bal4 <- selbal.cv(otu_genus_dif[,1:82],as.factor(otu_genus_dif[,105]))
Bal4$accuracy.nvar
Bal4$var.barplot
plot.new()
grid.draw(Bal4$global.plot)
plot.new()
plot.tab(Bal4$cv.tab)
Bal4$cv.accuracy; summary(Bal4$cv.accuracy)



# diet:
Bal2 <- selbal.cv(otu_genus_dif[,1:82],as.factor(otu_genus_dif[,84]))
Bal2$accuracy.nvar
Bal2$var.barplot
plot.new()
grid.draw(Bal2$global.plot)
plot.new()
plot.tab(Bal2$cv.tab)
Bal2$cv.accuracy
summary(Bal2$cv.accuracy)
Bal2$glm

# Hb1Ac:
Bal3 <- selbal(otu_genus_dif[,1:82],otu_genus_dif[,108], maxV = 20)
Bal3$accuracy.nvar
Bal3$var.barplot
plot.new()
grid.draw(Bal3$global.plot)
plot.new()
plot.tab(Bal3$cv.tab)
Bal3$cv.accuracy
summary(Bal3$cv.accuracy)


# create selbal balance :
s <- sqrt((5*6)/(5+6))
numerator <- (otu_genus_gbm$D_5__UBA1819 * otu_genus_gbm$D_5__.Ruminococcus..torques.group *
                otu_genus_gbm$D_5__Ruminiclostridium.5 * otu_genus_gbm$D_5__Ruminococcaceae.UCG.013 *
                otu_genus_gbm$unc_D_4__Ruminococcaceae)^(1/5)

denominator <- (otu_genus_gbm$D_5__Barnesiella * otu_genus_gbm$D_5__Defluviitaleaceae.UCG.011 *
                  otu_genus_gbm$D_5__Ruminococcaceae.UCG.003 * otu_genus_gbm$D_5__.Eubacterium..xylanophilum.group *
                  otu_genus_gbm$D_5__Butyricicoccus * otu_genus_gbm$unc_D_4__Peptococcaceae)^(1/6)
bal_selbal <- s * log(numerator/denominator)

# other algorithm:
bal_selb_2 <- 1/5 * (log(otu_genus_gbm$D_5__UBA1819) + log(otu_genus_gbm$D_5__.Ruminococcus..torques.group) +
                       log(otu_genus_gbm$D_5__Ruminiclostridium.5) + log(otu_genus_gbm$D_5__Ruminococcaceae.UCG.013) +
                       log(otu_genus_gbm$unc_D_4__Ruminococcaceae)) - 
              1/6 * (log(otu_genus_gbm$D_5__Barnesiella) + log(otu_genus_gbm$D_5__Defluviitaleaceae.UCG.011) + 
                       log(otu_genus_gbm$D_5__Ruminococcaceae.UCG.003) + log(otu_genus_gbm$D_5__.Eubacterium..xylanophilum.group) +
                       log(otu_genus_gbm$D_5__Butyricicoccus) + log(otu_genus_gbm$unc_D_4__Peptococcaceae))


# add balance:
otu_genus_gbm_with_selb <- cbind(otu_genus_gbm, bal_selbal)

A <-  otu_genus_gbm_with_selb[(otu_genus_gbm_with_selb$normalized_glycemia == 1 &
                                 otu_genus_gbm_with_selb$time == 0),]
B <- otu_genus_gbm_with_selb[(otu_genus_gbm_with_selb$normalized_glycemia == 1 &
                                otu_genus_gbm_with_selb$time == 12),]

C <-  otu_genus_gbm_with_selb[(otu_genus_gbm_with_selb$normalized_glycemia == 0 &
                                 otu_genus_gbm_with_selb$time == 0),]
D <- otu_genus_gbm_with_selb[(otu_genus_gbm_with_selb$normalized_glycemia == 0 &
                                otu_genus_gbm_with_selb$time == 12),]


wilcox.test(A$bal_selbal, B$bal_selbal, paired = T, alternative = "less")
wilcox.test(C$bal_selbal, D$bal_selbal, paired = T, alternative = "greater")

norm_glyc <- otu_genus_gbm_with_selb[(otu_genus_gbm_with_selb$normalized_glycemia == 1),]
diab <- otu_genus_gbm_with_selb[!(otu_genus_gbm_with_selb$normalized_glycemia == 1),]

r <- rmcorr(norm_glyc$ID, norm_glyc$Glucosa, norm_glyc$bal_selbal, norm_glyc)
plot(r)
r$p; r$r


q <- rmcorr(diab$ID, diab$Glucosa, diab$bal_selbal, diab)
plot(q)
q$p; q$r

a <- rmcorr(otu_genus_gbm_with_selb$ID, otu_genus_gbm_with_selb$Glucosa, 
            otu_genus_gbm_with_selb$bal_selbal, otu_genus_gbm_with_selb)
plot(a)
a$p; a$r

