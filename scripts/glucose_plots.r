library(dplyr)
library(xlsx)
library(ggpubr)
library(ggplot2)

otu_genus_with_metadata <- read.xlsx("/home/jakob/Documents/gut_diabetes/data/otu_genus_with_metadata.xlsx",1)

bef_norm <- filter(otu_genus_with_metadata, time == 0 & normalized_glycemia == 1) %>% select(Glucosa) %>% unlist()
aft_norm <- filter(otu_genus_with_metadata, time == 12 & normalized_glycemia == 1) %>% select(Glucosa) %>% unlist()
t.test(bef_norm, aft_norm, paired = T)

d <- data.frame(before = bef_norm, after = aft_norm)

bef_diab <- filter(otu_genus_with_metadata, time == 0 & normalized_glycemia == 0) %>% select(Glucosa) %>% unlist()
aft_diab <- filter(otu_genus_with_metadata, time == 12 & normalized_glycemia == 0) %>% select(Glucosa) %>% unlist()
t.test(bef_diab, aft_diab, paired = T)

e <- data.frame(before = bef_diab, after = aft_diab)

require(gridExtra)
p1 <- ggpaired(d, cond1 = "before", cond2 = "after",fill = "condition", 
         palette = "jco", title = "Normalized_glycemia patients") + geom_hline(yintercept = 100)
p2 <- ggpaired(e, cond1 = "before", cond2 = "after",fill = "condition", 
         palette = "jco", title = "Patients that stay prediabetic") + geom_hline(yintercept = 100)
grid.arrange(p1, p2, ncol=2) 
